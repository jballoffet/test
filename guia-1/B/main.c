/*!
 * @file   main.c
 * @brief  Guia de Ejercicios - Intrductroducción y Estructuras de Decision
 *                   Ejercicio  12
 * @author Arik Gaston Kenigsberg <akenigsberg@frba.utn.edu.ar>
 * @date   10/05/2021
 * @details Para Compilar, linkear y ejecutar:
 *          [Ingresar a la carpeta TpN°1A desde el terminal.
 *           Para compilar escribir el siguiente codigo :"gcc -c main.c"
 *           Para Linkear escribir el siguiente codigo: "gcc main.o -Wall -L. -linfo1 -o app"
 *           Para Ejecutar escribir el siguiente codigo: "./app"]
 */
#include <stdio.h>
#include "info1.h"

int main(void)
{
  int numero;
  int multiplo;

  numero = obtener_int ("Ingrese un numero entero\n");
  multiplo = numero / 10 * 10 ;

 printf("el multiplo de 10 mas cercano es %d\n",multiplo);

return 0;
}
