/*!
 * @file   main.c
 * @brief  Guia de Ejercicios - Intrductroducción y Estructuras de Decision
 *                   Ejercicio  3 
 * @author Arik Gaston Kenigsberg <akenigsberg@frba.utn.edu.ar>
 * @date   10/05/2021
 * @details Para Compilar, linkear y ejecutar:
 *          [Ingresar a la carpeta TpN°1A desde el terminal.
 *           Para compilar escribir el siguiente codigo :"gcc -c main.c"
 *           Para Linkear escribir el siguiente codigo: "gcc main.o -Wall -L. -linfo1 -o app"
 *           Para Ejecutar escribir el siguiente codigo: "./app"]
 */
#include <stdio.h>
#include "info1.h"

int main(void)
{
   
  int hora;
  int minutos;
  float segundos;
  float conversion;
  
  hora = obtener_int("Ingrese hora : ");
  minutos = obtener_int("Ingrese Minutos : ");
  segundos = obtener_float("Ingrese segundos : ") ;
  conversion = hora*3600 + minutos * 60 + segundos;
 
  if((hora >= 0) && (minutos >= 0) && (segundos >= 0))
  {
    printf("El Valor en Segundos es: %f seg\n", conversion);
  }
  else if((hora <= 0)&& (minutos <= 0) && (segundos <= 0))
  {
    printf ("El Valor en segundos es: %f seg\n", conversion);
  }
  else
  {
    printf ("Error en los signos reviselo e intentelo otra vez\n");
  }
return 0;
}
