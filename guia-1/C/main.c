/*!
 * @file   main.c
 * @brief  Guia de Ejercicios - Intrductroducción y Estructuras de Decision
 *                   Ejercicio  3 
 * @author Arik Gaston Kenigsberg <akenigsberg@frba.utn.edu.ar>
 * @date   10/05/2021
 * @details Para Compilar, linkear y ejecutar:
 *          [Ingresar a la carpeta TpN°1A desde el terminal.
 *           Para compilar escribir el siguiente codigo :"gcc -c main.c"
 *           Para Linkear escribir el siguiente codigo: "gcc main.o -Wall -L. -linfo1 -o app"
 *           Para Ejecutar escribir el siguiente codigo: "./app"]
 */
/* Aguante Game of thrones */
#include <stdio.h>
#include "info1.h"
int main(void)
{
   
  char caracter;

 caracter =  obtener_char("Ingrese un caracter\n");
 
  if(caracter == 'A')
  {
    printf("House Arryn\n");
  }
  else if(caracter == 'B')
  {
    printf ("House Baratheon\n");
  }
  else if(caracter =='F')
  {
    printf ("House Frey\n");
  }
   else if(caracter == 'G')
  {
    printf ("House Greyjoy\n");
  }
  else if(caracter == 'L')
  {
    printf ("House Lannister\n");
  }
  else if(caracter == 'M')
  {
    printf ("House Martell\n");
  }
  else if(caracter == 'S')
  {
    printf ("House Stark\n");
  }
  else if(caracter == 'T')
  {
    printf ("House Targaryen\n");
  }
 else
 {
 printf("No lo conozco");
 }

return 0;
}
